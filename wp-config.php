<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('REVISR_GIT_PATH', ''); // Added by Revisr
define('REVISR_WORK_TREE', 'D:\xampp\htdocs\hardik\wordpress-4.9.5\wordpress/'); // Added by Revisr
define('DB_NAME', 'wordpress_try');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9Yv5z!Pp=5bZH|r]{2CneUrV_AyX4x)B`aX9$T1$6f)c^]_3wMa5)N`$HUmjv,{!');
define('SECURE_AUTH_KEY',  'F5LxwfBS|K4lg{IN#Tn<^|U5U5`*Q+UlWHGJ@V?TWu_X^2^L?c;Z!9zy:E3L%&>1');
define('LOGGED_IN_KEY',    '@MXk1&FVM>hnM/VIDayc!*R5~B QN1m~:L 7H]F(#pEz!BfRl^BVz 4rq(iI=P4,');
define('NONCE_KEY',        'J4<[:$K(E>?=SzT=It:HFsH(8W@+wxFt.]GHX#+Ftop:JX}Xk6HArYh- tc@s5V<');
define('AUTH_SALT',        '3kb?i[pQigsHFU-t~Qo(T`||=b}`+<@x1fO~<Y9ey41/!0&U]vnD,V9#@LBU$Y-{');
define('SECURE_AUTH_SALT', '#gW4bF:8L3F9$7mk[V5LM8o<%wlVv6a&`N5<cs&-Abd7#Tdj^lC}yNblmGsIT>p>');
define('LOGGED_IN_SALT',   '[UWeZ|>LU9J4S^,k|vTK#40T 4X<Rs!88g[70L`;8)1UnpS!=e*Gx;!WY5RKIfL/');
define('NONCE_SALT',       'C1&.Dn9)E1!{;qL2X_ex@V{EJy<Ds9yN#G?~7=iDgW/fI[h_lCQWKw9Zr8Z`~V>{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
