<?php
/*
Plugin Name: Meta Tags
Plugin URI: https://divpusher.com/
Description: A super simple plugin to edit meta tags on all your posts and pages for SEO. Facebook's OpenGraph and Twitter Cards are included.
Version: 1.2.4
Tags: meta tags, seo, edit meta tags, search engine optimization, facebook open graph, twitter cards, schema.org
Text Domain: meta-tags
Author: DivPusher - WordPress Theme Club
Author URI: https://divpusher.com
*/



	//better safe than sorry
		if (!function_exists('add_action')){
			echo esc_html__('Hi there! I\'m just a plugin, not much I can do when called directly.','meta-tags');
			exit;
		}

		
		
	//plugin main file path
		define( 'DP_META_TAGS_PLUGIN_FILE', __FILE__ );
		
		
		
	//add settings link to plugin page
		function dp_metatags_actions( $links, $file ) {
			if( $file == plugin_basename( DP_META_TAGS_PLUGIN_FILE ) && function_exists( 'admin_url' ) ) {
				$settings_link = '<a href="' . admin_url( 'options-general.php?page=meta-tags-options' ) . '">' . __('Set up tags','meta-tags') . '</a>';				
				array_unshift( $links, $settings_link );
			}
			return $links;
		}
		add_filter( 'plugin_action_links', 'dp_metatags_actions', 10, 2 );
	
	
	
	//add notice to theme page
		function dp_metatags_notice() {				
			global $pagenow;			
			
			if($pagenow == 'theme-install.php' || $pagenow == 'themes.php'){
				echo '<div class="notice notice-success is-dismissible">
					<p>'.__('Need some nice, free or premium theme? <a href="https://divpusher.com" target="_blank">Have a look around here!','meta-tags').'</a></p>			
				</div>';			
			}
		}
		add_action( 'admin_notices', 'dp_metatags_notice' );
		
		
		
	//add notice on plugin activation				
		register_activation_hook( __FILE__, 'dp_metatags_notice_activation_hook' );		 
		function dp_metatags_notice_activation_hook() {
			set_transient( 'dp-metatags-activation-notice', true, 5 );
		}
		 
		 
		add_action( 'admin_notices', 'dp_metatags_activation_notice' );		 
		function dp_metatags_activation_notice(){			
			if( get_transient( 'dp-metatags-activation-notice' ) ){
				
				echo '<div class="updated notice is-dismissible">
					<p>'.__('Thank you for using our plugin. In case you need some nice, free or premium theme, have a <a href="https://divpusher.com" target="_blank">look around here!','meta-tags').'</a></p>			
				</div>';
				
				delete_transient( 'dp-metatags-activation-notice' );
			}
		}	
		
		
		
	
	
	
	//settings page in admin		
		function dp_metatags_admin_option(){		
				
			 add_submenu_page(
				'options-general.php',
				esc_html__( 'Meta tags', 'meta-tags' ),
				esc_html__( 'Meta tags', 'meta-tags' ),
				'manage_options',
				'meta-tags-options',
				'dp_metatags_settings_page'
			);
			
		}
		add_action( 'admin_menu', 'dp_metatags_admin_option' );
		
		
		function dp_metatags_settings_page(){			
		
			//check user permission
				if(!current_user_can('administrator')){				
					return;
				}

			
			if(!empty($_POST['submit']) && check_admin_referer('dp_metatags_save_settings', 'dp-metatags-nonce')){
				//save changes
				
				if(!empty($_POST['dp-metatags-general-description'])){ update_option('dp-metatags-general-description',sanitize_text_field($_POST['dp-metatags-general-description'])); }
				if(!empty($_POST['dp-metatags-general-keywords'])){ update_option('dp-metatags-general-keywords',sanitize_text_field($_POST['dp-metatags-general-keywords'])); }
				if(!empty($_POST['dp-metatags-general-title'])){ update_option('dp-metatags-general-title',sanitize_text_field($_POST['dp-metatags-general-title'])); }
				
				if(!empty($_POST['dp-metatags-og-title'])){ update_option('dp-metatags-og-title',sanitize_text_field($_POST['dp-metatags-og-title'])); }
				if(!empty($_POST['dp-metatags-og-type'])){ update_option('dp-metatags-og-type',sanitize_text_field($_POST['dp-metatags-og-type'])); }
				if(!empty($_POST['dp-metatags-og-audio'])){ update_option('dp-metatags-og-audio',esc_url($_POST['dp-metatags-og-audio'])); }
				if(!empty($_POST['dp-metatags-og-image'])){ update_option('dp-metatags-og-image',esc_url($_POST['dp-metatags-og-image'])); }
				if(!empty($_POST['dp-metatags-og-video'])){ update_option('dp-metatags-og-video',esc_url($_POST['dp-metatags-og-video'])); }
				if(!empty($_POST['dp-metatags-og-url'])){ update_option('dp-metatags-og-url',esc_url($_POST['dp-metatags-og-url'])); }
				if(!empty($_POST['dp-metatags-og-description'])){ update_option('dp-metatags-og-description',sanitize_text_field($_POST['dp-metatags-og-description'])); }
				
				if(!empty($_POST['dp-metatags-twitter-card'])){ update_option('dp-metatags-twitter-card',sanitize_text_field($_POST['dp-metatags-twitter-card'])); }
				if(!empty($_POST['dp-metatags-twitter-title'])){ update_option('dp-metatags-twitter-title',sanitize_text_field($_POST['dp-metatags-twitter-title'])); }
				if(!empty($_POST['dp-metatags-twitter-description'])){ update_option('dp-metatags-twitter-description',sanitize_text_field($_POST['dp-metatags-twitter-description'])); }
				if(!empty($_POST['dp-metatags-twitter-image'])){ update_option('dp-metatags-twitter-image',esc_url($_POST['dp-metatags-twitter-image'])); }
				
				$allowed_html = array(
					'meta' => array(
						'name' => array(),
						'property' => array(),
						'content' => array(),						
						'http-equiv' => array()
					)
				);
				if(!empty($_POST['dp-metatags-custom'])){ update_option('dp-metatags-custom',wp_kses( $_POST['dp-metatags-custom'], $allowed_html )); }
				
			}
		
			$dp_metatags_general_description = get_option('dp-metatags-general-description');
			$dp_metatags_general_keywords = get_option('dp-metatags-general-keywords');
			$dp_metatags_general_title = get_option('dp-metatags-general-title');
			
			$dp_metatags_og_title = get_option('dp-metatags-og-title');
			$dp_metatags_og_type = get_option('dp-metatags-og-type');
			$dp_metatags_og_audio = get_option('dp-metatags-og-audio');
			$dp_metatags_og_image = get_option('dp-metatags-og-image');
			$dp_metatags_og_video = get_option('dp-metatags-og-video');
			$dp_metatags_og_url = get_option('dp-metatags-og-url');
			$dp_metatags_og_description = get_option('dp-metatags-og-description');
			
			$dp_metatags_twitter_card = get_option('dp-metatags-twitter-card');			
			$dp_metatags_twitter_title = get_option('dp-metatags-twitter-title');
			$dp_metatags_twitter_description = get_option('dp-metatags-twitter-description');
			$dp_metatags_twitter_image = get_option('dp-metatags-twitter-image');
			
			$dp_metatags_custom = get_option('dp-metatags-custom');
			
			$page_on_front = get_option('page_on_front');
		
		
			echo '<h1>'.esc_html__('Meta Tags','meta-tags').'</h1>';
			
			if($page_on_front == '0'){
				//frontpage shows latest posts				
				echo '<p>'.__('It seems the frontpage shows your latest posts (based on <b>Settings - Reading</b>). Here you can set up meta tags for the frontpage.').'<br />'
				.__('For the rest please visit the page/post editor where you can add specific meta tags for each of them in the <b>Meta Tag Editor</b> box.','meta-tags').'</p>
				
				<p>&nbsp;</p>
				
				<form method="post" action="options-general.php?page=meta-tags-options" novalidate="novalidate">';
				
				
				//add nonce
					wp_nonce_field( 'dp_metatags_save_settings', 'dp-metatags-nonce' );
				
				
				//general meta tags
					echo'
					<h2 class="title">'.esc_html__('General meta tags','meta-tags').'</h2>
					
					<div style="margin-left: 20px;">
						<p><label for="dp-metatags-general-description"><b>'.__('Description','meta-tags').'</b></label><br /><span class="description">'.__('This text will appear below your title in Google search results. Describe this page/post in 155 maximum characters. Note: Google will not consider this in its search ranking algorithm.','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-general-description" name="dp-metatags-general-description" class="regular-text" value="'.(!empty($dp_metatags_general_description) ? esc_attr($dp_metatags_general_description) : '').'" /></p>
						
						<p><label for="dp-metatags-general-keywords"><b>'.__('Keywords','meta-tags').'</b></label><br /><span class="description">'.__('Improper or spammy use most likely will hurt you with some search engines. Google will not consider this in its search ranking algorithm, so it\'s not really recommended.','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-general-keywords" name="dp-metatags-general-keywords" class="regular-text" value="'.(!empty($dp_metatags_general_keywords) ? esc_attr($dp_metatags_general_keywords) : '').'" /></p>
						
						<p><label for="dp-metatags-general-title"><b>'.__('Page title','meta-tags').'</b></label><br /><span class="description">'.__('Make page titles as keyword-relevant as possible and up to 70 characters. Longer titles are oftentimes chopped down or rewritten algorithmically.','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-general-title" name="dp-metatags-general-title" class="regular-text" value="'.(!empty($dp_metatags_general_title) ? esc_attr($dp_metatags_general_title) : '').'" /></p>
					</div>
					
					<p>&nbsp;</p>
					<hr />
					';
			
			
				//Facebook's OpenGraph meta tags
					echo '
					<h2 class="title">'.esc_html__('Facebook\'s OpenGraph meta tags','meta-tags').'</h2>
					<p>'.esc_html__('Open Graph has become very popular, so most social networks default to Open Graph if no other meta tags are present.','meta-tags').'</p>
					
					<div style="margin-left: 20px;">
						<p><label for="dp-metatags-og-title"><b>'.__('Title','meta-tags').'</b></label><br /><span class="description">'.__('The headline.','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-og-title" name="dp-metatags-og-title" class="regular-text" value="'.(!empty($dp_metatags_og_title) ? esc_attr($dp_metatags_og_title) : '').'" /></p>
						
						<p><label for="dp-metatags-og-type"><b>'.__('Type','meta-tags').'</b></label><br /><span class="description">'.__('Article, website or other. Here is a list of all available types: <a href="http://ogp.me/#types" target="_blank">http://ogp.me/#types</a>','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-og-type" name="dp-metatags-og-type" class="regular-text" value="'.(!empty($dp_metatags_og_type) ? esc_attr($dp_metatags_og_type) : '').'" /></p>
						
						<p><label for="dp-metatags-og-audio"><b>'.__('Audio','meta-tags').'</b></label><br /><span class="description">'.__('URL to your content\'s audio.','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-og-audio" name="dp-metatags-og-audio" class="regular-text" value="'.(!empty($dp_metatags_og_audio) ? esc_attr($dp_metatags_og_audio) : '').'" /></p>
						
						<p><label for="dp-metatags-og-image"><b>'.__('Image','meta-tags').'</b></label><br /><span class="description">'.__('URL to your content\'s image. It should be at least 600x315 pixels, but 1200x630 or larger is preferred (up to 5MB). Stay close to a 1.91:1 aspect ratio to avoid cropping.','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-og-image" name="dp-metatags-og-image" class="regular-text" value="'.(!empty($dp_metatags_og_image) ? esc_attr($dp_metatags_og_image) : '').'" /></p>
						
						<p><label for="dp-metatags-og-video"><b>'.__('Video','meta-tags').'</b></label><br /><span class="description">'.__('URL to your content\'s video. Videos need an og:image tag to be displayed in News Feed.','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-og-video" name="dp-metatags-og-video" class="regular-text" value="'.(!empty($dp_metatags_og_video) ? esc_attr($dp_metatags_og_video) : '').'" /></p>
						
						<p><label for="dp-metatags-og-url"><b>'.__('URL','meta-tags').'</b></label><br /><span class="description">'.__('The URL of your page. Use the canonical URL for this tag (the search engine friendly URL that you want the search engines to treat as authoritative).','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-og-url" name="dp-metatags-og-url" class="regular-text" value="'.(!empty($dp_metatags_og_url) ? esc_attr($dp_metatags_og_url) : '').'" /></p>
						
						<p><label for="dp-metatags-og-description"><b>'.__('Description','meta-tags').'</b></label><br /><span class="description">'.__('A short summary about the content.','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-og-description" name="dp-metatags-og-description" class="regular-text" value="'.(!empty($dp_metatags_og_description) ? esc_attr($dp_metatags_og_description) : '').'" /></p>
						
					</div>
					
					<p>&nbsp;</p>
					<hr />
					';
				
				
				//Twitter meta tags
					echo '
					<h2 class="title">'.esc_html__('Twitter cards','meta-tags').'</h2>
					
					<div style="margin-left: 20px;">
						<p><label for="dp-metatags-twitter-card"><b>'.__('Card','meta-tags').'</b></label><br /><span class="description">'.__('This is the card type. Your options are summary, photo or player. Twitter will default to "summary" if it is not specified.','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-twitter-card" name="dp-metatags-twitter-card" class="regular-text" value="'.(!empty($dp_metatags_twitter_card) ? esc_attr($dp_metatags_twitter_card) : '').'" /></p>
						
						<p><label for="dp-metatags-twitter-title"><b>'.__('Title','meta-tags').'</b></label><br /><span class="description">'.__('A concise title for the related content.','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-twitter-title" name="dp-metatags-twitter-title" class="regular-text" value="'.(!empty($dp_metatags_twitter_title) ? esc_attr($dp_metatags_twitter_title) : '').'" /></p>
						
						<p><label for="dp-metatags-twitter-description"><b>'.__('Description','meta-tags').'</b></label><br /><span class="description">'.__('Summary of content.','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-twitter-description" name="dp-metatags-twitter-description" class="regular-text" value="'.(!empty($dp_metatags_twitter_description) ? esc_attr($dp_metatags_twitter_description) : '').'" /></p>					
						
						<p><label for="dp-metatags-twitter-image"><b>'.__('Image','meta-tags').'</b></label><br /><span class="description">'.__('Image representing the content. Use aspect ratio of 1:1 with minimum dimensions of 144x144 or maximum of 4096x4096 pixels. Images must be less than 5MB in size.','meta-tags').'</span></p>
						<p><input type="text" id="dp-metatags-twitter-image" name="dp-metatags-twitter-image" class="regular-text" value="'.(!empty($dp_metatags_twitter_image) ? esc_attr($dp_metatags_twitter_image) : '').'" /></p>
						
					</div>
					
					<p>&nbsp;</p>
					<hr />
					';
							
				
				//Custom meta tags 			
					echo '
					<h2 class="title">'.esc_html__('Custom meta tags','meta-tags').'</h2>			
					
					<div style="margin-left: 20px;">					
						<textarea id="dp-metatags-custom" name="dp-metatags-custom" class="regular-text code">'.(!empty($dp_metatags_custom) ? esc_textarea($dp_metatags_custom) : '').'</textarea>
					</div>
					
					
					<p class="submit"><input name="submit" id="submit" class="button button-primary" value="'.__('Save Changes','meta-tags').'" type="submit"></p>
					</form>
					';
				
			}else{
				//frontpage shows a specific page				
				echo '<p>'.__('Go to your page/post editor and you will find a new <b>Meta Tag Editor</b> box where you can add specific meta tags for each of them.','meta-tags').'</p>';
			}
			
			
		}
	
		
	
	
	//register metabox
		function dp_metatags_metabox(){	
			if(function_exists('add_meta_box')){		
				add_meta_box( 'dp-metatags', esc_html__('Meta Tag Editor','meta-tags'), 'dp_metatags_editor', 'page', 'normal' );						
				add_meta_box( 'dp-metatags', esc_html__('Meta Tag Editor','meta-tags'), 'dp_metatags_editor', 'post', 'normal' );						
				add_meta_box( 'dp-metatags', esc_html__('Meta Tag Editor','meta-tags'), 'dp_metatags_editor', 'product', 'normal', 'low' );						
			}
		}		
		add_action('admin_menu', 'dp_metatags_metabox');
	
	

	//meta tag editor 
		function dp_metatags_editor(){			
			global $post;
		
			//load saved values
			$dp_metatags_general_description = get_post_meta($post->ID, 'dp-metatags-general-description', true);
			$dp_metatags_general_keywords = get_post_meta($post->ID, 'dp-metatags-general-keywords', true);
			$dp_metatags_general_title = get_post_meta($post->ID, 'dp-metatags-general-title', true);
			$dp_metatags_og_title = get_post_meta($post->ID, 'dp-metatags-og-title', true);
			$dp_metatags_og_type = get_post_meta($post->ID, 'dp-metatags-og-type', true);
			$dp_metatags_og_audio = get_post_meta($post->ID, 'dp-metatags-og-audio', true);
			$dp_metatags_og_image = get_post_meta($post->ID, 'dp-metatags-og-image', true);
			$dp_metatags_og_video = get_post_meta($post->ID, 'dp-metatags-og-video', true);
			$dp_metatags_og_url = get_post_meta($post->ID, 'dp-metatags-og-url', true);
			$dp_metatags_og_description = get_post_meta($post->ID, 'dp-metatags-og-description', true);
			$dp_metatags_twitter_card = get_post_meta($post->ID, 'dp-metatags-twitter-card', true);			
			$dp_metatags_twitter_title = get_post_meta($post->ID, 'dp-metatags-twitter-title', true);
			$dp_metatags_twitter_description = get_post_meta($post->ID, 'dp-metatags-twitter-description', true);
			$dp_metatags_twitter_image = get_post_meta($post->ID, 'dp-metatags-twitter-image', true);
			$dp_metatags_custom = get_post_meta($post->ID, 'dp-metatags-custom', true);
		
		
			//general meta tags
				echo'
				<p><b>'.esc_html__('General meta tags','meta-tags').'</b></p>
				
				<div style="margin-left: 20px;">
					<p><label for="dp-metatags-general-description"><b>'.__('Description','meta-tags').'</b></label><br /><span class="description">'.__('This text will appear below your title in Google search results. Describe this page/post in 155 maximum characters. Note: Google will not consider this in its search ranking algorithm.','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-general-description" name="dp-metatags-general-description" class="regular-text" value="'.(!empty($dp_metatags_general_description) ? esc_attr($dp_metatags_general_description) : '').'" /></p>
					
					<p><label for="dp-metatags-general-keywords"><b>'.__('Keywords','meta-tags').'</b></label><br /><span class="description">'.__('Improper or spammy use most likely will hurt you with some search engines. Google will not consider this in its search ranking algorithm, so it\'s not really recommended.','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-general-keywords" name="dp-metatags-general-keywords" class="regular-text" value="'.(!empty($dp_metatags_general_keywords) ? esc_attr($dp_metatags_general_keywords) : '').'" /></p>
					
					<p><label for="dp-metatags-general-title"><b>'.__('Page title','meta-tags').'</b></label><br /><span class="description">'.__('Make page titles as keyword-relevant as possible and up to 70 characters. Longer titles are oftentimes chopped down or rewritten algorithmically.','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-general-title" name="dp-metatags-general-title" class="regular-text" value="'.(!empty($dp_metatags_general_title) ? esc_attr($dp_metatags_general_title) : '').'" /></p>
				</div>
				
				<p>&nbsp;</p>
				<hr />
				';
			
			
			//Facebook's OpenGraph meta tags
				echo '
				<p><b>'.esc_html__('Facebook\'s OpenGraph meta tags','meta-tags').'</b></p>
				<p>'.esc_html__('Open Graph has become very popular, so most social networks default to Open Graph if no other meta tags are present.','meta-tags').'</p>
				
				<div style="margin-left: 20px;">
					<p><label for="dp-metatags-og-title"><b>'.__('Title','meta-tags').'</b></label><br /><span class="description">'.__('The headline.','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-og-title" name="dp-metatags-og-title" class="regular-text" value="'.(!empty($dp_metatags_og_title) ? esc_attr($dp_metatags_og_title) : '').'" /></p>
					
					<p><label for="dp-metatags-og-type"><b>'.__('Type','meta-tags').'</b></label><br /><span class="description">'.__('Article, website or other. Here is a list of all available types: <a href="http://ogp.me/#types" target="_blank">http://ogp.me/#types</a>','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-og-type" name="dp-metatags-og-type" class="regular-text" value="'.(!empty($dp_metatags_og_type) ? esc_attr($dp_metatags_og_type) : '').'" /></p>
					
					<p><label for="dp-metatags-og-audio"><b>'.__('Audio','meta-tags').'</b></label><br /><span class="description">'.__('URL to your content\'s audio.','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-og-audio" name="dp-metatags-og-audio" class="regular-text" value="'.(!empty($dp_metatags_og_audio) ? esc_attr($dp_metatags_og_audio) : '').'" /></p>
					
					<p><label for="dp-metatags-og-image"><b>'.__('Image','meta-tags').'</b></label><br /><span class="description">'.__('URL to your content\'s image. It should be at least 600x315 pixels, but 1200x630 or larger is preferred (up to 5MB). Stay close to a 1.91:1 aspect ratio to avoid cropping.','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-og-image" name="dp-metatags-og-image" class="regular-text" value="'.(!empty($dp_metatags_og_image) ? esc_attr($dp_metatags_og_image) : '').'" /></p>
					
					<p><label for="dp-metatags-og-video"><b>'.__('Video','meta-tags').'</b></label><br /><span class="description">'.__('URL to your content\'s video. Videos need an og:image tag to be displayed in News Feed.','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-og-video" name="dp-metatags-og-video" class="regular-text" value="'.(!empty($dp_metatags_og_video) ? esc_attr($dp_metatags_og_video) : '').'" /></p>
					
					<p><label for="dp-metatags-og-url"><b>'.__('URL','meta-tags').'</b></label><br /><span class="description">'.__('The URL of your page. Use the canonical URL for this tag (the search engine friendly URL that you want the search engines to treat as authoritative).','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-og-url" name="dp-metatags-og-url" class="regular-text" value="'.(!empty($dp_metatags_og_url) ? esc_attr($dp_metatags_og_url) : '').'" /></p>
					
					<p><label for="dp-metatags-og-description"><b>'.__('Description','meta-tags').'</b></label><br /><span class="description">'.__('A short summary about the content.','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-og-description" name="dp-metatags-og-description" class="regular-text" value="'.(!empty($dp_metatags_og_description) ? esc_attr($dp_metatags_og_description) : '').'" /></p>
					
				</div>
				
				<p>&nbsp;</p>
				<hr />
				';
			
			
			//Twitter meta tags
				echo '
				<p><b>'.esc_html__('Twitter cards','meta-tags').'</b></p>
				
				<div style="margin-left: 20px;">
					<p><label for="dp-metatags-twitter-card"><b>'.__('Card','meta-tags').'</b></label><br /><span class="description">'.__('This is the card type. Your options are summary, photo or player. Twitter will default to "summary" if it is not specified.','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-twitter-card" name="dp-metatags-twitter-card" class="regular-text" value="'.(!empty($dp_metatags_twitter_card) ? esc_attr($dp_metatags_twitter_card) : '').'" /></p>
					
					<p><label for="dp-metatags-twitter-title"><b>'.__('Title','meta-tags').'</b></label><br /><span class="description">'.__('A concise title for the related content.','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-twitter-title" name="dp-metatags-twitter-title" class="regular-text" value="'.(!empty($dp_metatags_twitter_title) ? esc_attr($dp_metatags_twitter_title) : '').'" /></p>
					
					<p><label for="dp-metatags-twitter-description"><b>'.__('Description','meta-tags').'</b></label><br /><span class="description">'.__('Summary of content.','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-twitter-description" name="dp-metatags-twitter-description" class="regular-text" value="'.(!empty($dp_metatags_twitter_description) ? esc_attr($dp_metatags_twitter_description) : '').'" /></p>					
					
					<p><label for="dp-metatags-twitter-image"><b>'.__('Image','meta-tags').'</b></label><br /><span class="description">'.__('Image representing the content. Use aspect ratio of 1:1 with minimum dimensions of 144x144 or maximum of 4096x4096 pixels. Images must be less than 5MB in size.','meta-tags').'</span></p>
					<p><input type="text" id="dp-metatags-twitter-image" name="dp-metatags-twitter-image" class="regular-text" value="'.(!empty($dp_metatags_twitter_image) ? esc_attr($dp_metatags_twitter_image) : '').'" /></p>
					
				</div>
				
				<p>&nbsp;</p>
				<hr />
				';
						
			
			//Custom meta tags 			
				echo '
				<p><b>'.esc_html__('Custom meta tags','meta-tags').'</b></p>			
				
				<div style="margin-left: 20px;">					
					<textarea id="dp-metatags-custom" name="dp-metatags-custom" class="regular-text code">'.(!empty($dp_metatags_custom) ? esc_textarea($dp_metatags_custom) : '').'</textarea>
				</div>
				';
				
			
			echo '<p>&nbsp;</p>';
		}
		
		
		
	//save tags
		function dp_metatags_save($post_id){						
			
			if(empty($post_id)){
				return;
			}


			//check post type
			$post_type = get_post_type($post_id);
			if('page' != $post_type && 'post' != $post_type && 'product' != $post_type){
				return;
			}



			if(!empty($_POST['dp-metatags-general-description'])){
				update_post_meta($post_id,'dp-metatags-general-description',sanitize_text_field($_POST['dp-metatags-general-description']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-general-description'); 
			}
			
			if(!empty($_POST['dp-metatags-general-keywords'])){	
				update_post_meta($post_id,'dp-metatags-general-keywords',sanitize_text_field($_POST['dp-metatags-general-keywords']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-general-keywords'); 
			}
			
			if(!empty($_POST['dp-metatags-general-title'])){	
				update_post_meta($post_id,'dp-metatags-general-title',sanitize_text_field($_POST['dp-metatags-general-title']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-general-title'); 
			}			
			
			
			
			if(!empty($_POST['dp-metatags-og-title'])){	
				update_post_meta($post_id,'dp-metatags-og-title',sanitize_text_field($_POST['dp-metatags-og-title']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-og-title'); 
			}
			
			if(!empty($_POST['dp-metatags-og-type'])){	
				update_post_meta($post_id,'dp-metatags-og-type',sanitize_text_field($_POST['dp-metatags-og-type']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-og-type'); 
			}
			
			if(!empty($_POST['dp-metatags-og-audio'])){	
				update_post_meta($post_id,'dp-metatags-og-audio',esc_url($_POST['dp-metatags-og-audio']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-og-audio'); 
			}
			
			if(!empty($_POST['dp-metatags-og-image'])){	
				update_post_meta($post_id,'dp-metatags-og-image',esc_url($_POST['dp-metatags-og-image']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-og-image'); 
			}
			
			if(!empty($_POST['dp-metatags-og-video'])){	
				update_post_meta($post_id,'dp-metatags-og-video',esc_url($_POST['dp-metatags-og-video']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-og-video'); 
			}
			
			if(!empty($_POST['dp-metatags-og-url'])){	
				update_post_meta($post_id,'dp-metatags-og-url',esc_url($_POST['dp-metatags-og-url']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-og-url'); 
			}
			
			if(!empty($_POST['dp-metatags-og-description'])){	
				update_post_meta($post_id,'dp-metatags-og-description',sanitize_text_field($_POST['dp-metatags-og-description']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-og-description'); 
			}
			
			
			
			
			
			if(!empty($_POST['dp-metatags-twitter-card'])){	
				update_post_meta($post_id,'dp-metatags-twitter-card',sanitize_text_field($_POST['dp-metatags-twitter-card']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-twitter-card'); 
			}
			
			if(!empty($_POST['dp-metatags-twitter-title'])){	
				update_post_meta($post_id,'dp-metatags-twitter-title',sanitize_text_field($_POST['dp-metatags-twitter-title']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-twitter-title'); 
			}
			
			if(!empty($_POST['dp-metatags-twitter-description'])){	
				update_post_meta($post_id,'dp-metatags-twitter-description',sanitize_text_field($_POST['dp-metatags-twitter-description']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-twitter-description'); 
			}
			
			if(!empty($_POST['dp-metatags-twitter-image'])){	
				update_post_meta($post_id,'dp-metatags-twitter-image',esc_url($_POST['dp-metatags-twitter-image']));
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-twitter-image'); 
			}
			
			
			
			if(!empty($_POST['dp-metatags-custom'])){	
				$allowed_html = array(
					'meta' => array(
						'name' => array(),
						'property' => array(),
						'content' => array(),						
						'http-equiv' => array()
					)
				);
				
				update_post_meta($post_id,'dp-metatags-custom',wp_kses( $_POST['dp-metatags-custom'], $allowed_html ) );
			}elseif(!empty($post_id)){								
				delete_post_meta($post_id,'dp-metatags-custom'); 
			}
			
			
			
			
			
		}
		add_action('save_post', 'dp_metatags_save');
	
	
	
	//frontend echo
		function dp_metatags_echo(){
			global $post;
			
			$page_on_front = get_option('page_on_front');
		
		
			echo '			
	<!-- META TAGS PLUGIN START -->';
			
			if($page_on_front == 0 && is_front_page()){
			//latest posts on front page
				$dp_metatags_general_description = get_option('dp-metatags-general-description');
				$dp_metatags_general_keywords = get_option('dp-metatags-general-keywords');
				$dp_metatags_general_title = get_option('dp-metatags-general-title');
				
				$dp_metatags_og_title = get_option('dp-metatags-og-title');
				$dp_metatags_og_type = get_option('dp-metatags-og-type');
				$dp_metatags_og_audio = get_option('dp-metatags-og-audio');
				$dp_metatags_og_image = get_option('dp-metatags-og-image');
				$dp_metatags_og_video = get_option('dp-metatags-og-video');
				$dp_metatags_og_url = get_option('dp-metatags-og-url');
				$dp_metatags_og_description = get_option('dp-metatags-og-description');
				
				$dp_metatags_twitter_card = get_option('dp-metatags-twitter-card');			
				$dp_metatags_twitter_title = get_option('dp-metatags-twitter-title');
				$dp_metatags_twitter_description = get_option('dp-metatags-twitter-description');
				$dp_metatags_twitter_image = get_option('dp-metatags-twitter-image');
				
				$dp_metatags_custom = get_option('dp-metatags-custom');
				
				
				
			
				if(!empty($dp_metatags_general_description)){ echo '
	<meta name="description" content="'.esc_attr($dp_metatags_general_description).'" />'; }

				if(!empty($dp_metatags_general_keywords)){ echo '
	<meta name="keywords" content="'.esc_attr($dp_metatags_general_keywords).'" />'; }				
				if(!empty($dp_metatags_general_title)){ 					
					add_filter('pre_get_document_title', 'dp_metatags_title');
					function dp_metatags_title($title) {			
						global $post;
						$dp_metatags_general_title = get_option('dp-metatags-general-title');
						return esc_html($dp_metatags_general_title);						
					}				
				}
				
				
				if(!empty($dp_metatags_og_title)){ echo '
	<meta property="og:title" content="'.esc_attr($dp_metatags_og_title).'" />'; }

				if(!empty($dp_metatags_og_type)){ echo '
	<meta property="og:type" content="'.esc_attr($dp_metatags_og_type).'" />'; }
				
				if(!empty($dp_metatags_og_audio)){ echo '
	<meta property="og:audio" content="'.esc_attr($dp_metatags_og_audio).'" />'; }
				
				if(!empty($dp_metatags_og_image)){ echo '
	<meta property="og:image" content="'.esc_attr($dp_metatags_og_image).'" />'; }
				
				if(!empty($dp_metatags_og_video)){ echo '
	<meta property="og:video" content="'.esc_attr($dp_metatags_og_video).'" />'; }
				
				if(!empty($dp_metatags_og_url)){ echo '
	<meta property="og:url" content="'.esc_attr($dp_metatags_og_url).'" />'; }
				
				if(!empty($dp_metatags_og_description)){ echo '
	<meta property="og:description" content="'.esc_attr($dp_metatags_og_description).'" />'; }
				
				
				
				if(!empty($dp_metatags_twitter_card)){ echo '
	<meta name="twitter:card" content="'.esc_attr($dp_metatags_twitter_card).'" />'; }
				
				if(!empty($dp_metatags_twitter_title)){ echo '
	<meta name="twitter:title" content="'.esc_attr($dp_metatags_twitter_title).'" />'; }
				
				if(!empty($dp_metatags_twitter_description)){ echo '
	<meta name="twitter:description" content="'.esc_attr($dp_metatags_twitter_description).'" />'; }
				
				if(!empty($dp_metatags_twitter_image)){ echo '
	<meta name="twitter:image" content="'.esc_attr($dp_metatags_twitter_image).'" />'; }
				
				
				if(!empty($dp_metatags_custom)){ 
					$allowed_html = array(
						'meta' => array(
							'name' => array(),
							'property' => array(),
							'content' => array(),						
							'http-equiv' => array()
						)
					);
					
					echo '
	'.wp_kses( $dp_metatags_custom, $allowed_html );					
				}
				
				
				
				
				
			}else{	
			//load actual page settings
				
				//woocommerce hack to show proper ID
					if(class_exists('WooCommerce')){
						if(is_shop()){
							$post->ID = get_option('woocommerce_shop_page_id');
							
						}elseif(is_cart()){
							$post->ID = get_option('woocommerce_cart_page_id');
							
						}elseif(is_checkout()){
							$post->ID = get_option('woocommerce_checkout_page_id');
							
						}elseif(is_account_page()){
							$post->ID = get_option('woocommerce_myaccount_page_id');
						}
						
					}
			


				//check if current page is set as Posts page in Settings / Reading				
					if(is_home()){
						$post->ID = get_option('page_for_posts');
					} 


			
				if(!empty($post->ID)){
					$dp_metatags_general_description = get_post_meta($post->ID, 'dp-metatags-general-description', true);
					$dp_metatags_general_keywords = get_post_meta($post->ID, 'dp-metatags-general-keywords', true);
					$dp_metatags_general_title = get_post_meta($post->ID, 'dp-metatags-general-title', true);
					
					$dp_metatags_og_title = get_post_meta($post->ID, 'dp-metatags-og-title', true);
					$dp_metatags_og_type = get_post_meta($post->ID, 'dp-metatags-og-type', true);
					$dp_metatags_og_audio = get_post_meta($post->ID, 'dp-metatags-og-audio', true);
					$dp_metatags_og_image = get_post_meta($post->ID, 'dp-metatags-og-image', true);
					$dp_metatags_og_video = get_post_meta($post->ID, 'dp-metatags-og-video', true);
					$dp_metatags_og_url = get_post_meta($post->ID, 'ddp-metatags-og-url', true);
					$dp_metatags_og_description = get_post_meta($post->ID, 'dp-metatags-og-description', true);
					
					$dp_metatags_twitter_card = get_post_meta($post->ID, 'dp-metatags-twitter-card', true);			
					$dp_metatags_twitter_title = get_post_meta($post->ID, 'dp-metatags-twitter-title', true);
					$dp_metatags_twitter_description = get_post_meta($post->ID, 'dp-metatags-twitter-description', true);
					$dp_metatags_twitter_image = get_post_meta($post->ID, 'dp-metatags-twitter-image', true);
					
					$dp_metatags_custom = get_post_meta($post->ID, 'dp-metatags-custom', true);
				}
				
				
			
				if(!empty($dp_metatags_general_description)){ echo '
	<meta name="description" content="'.esc_attr($dp_metatags_general_description).'" />'; }

				if(!empty($dp_metatags_general_keywords)){ echo '
	<meta name="keywords" content="'.esc_attr($dp_metatags_general_keywords).'" />'; }				
				if(!empty($dp_metatags_general_title)){ 					
					add_filter('pre_get_document_title', 'dp_metatags_title');
					function dp_metatags_title($title) {			
						global $post;
						$dp_metatags_general_title = get_post_meta($post->ID, 'dp-metatags-general-title', true);
						return esc_html($dp_metatags_general_title);						
					}				
				}
				
				
				if(!empty($dp_metatags_og_title)){ echo '
	<meta property="og:title" content="'.esc_attr($dp_metatags_og_title).'" />'; }

				if(!empty($dp_metatags_og_type)){ echo '
	<meta property="og:type" content="'.esc_attr($dp_metatags_og_type).'" />'; }
				
				if(!empty($dp_metatags_og_audio)){ echo '
	<meta property="og:audio" content="'.esc_attr($dp_metatags_og_audio).'" />'; }
				
				if(!empty($dp_metatags_og_image)){ echo '
	<meta property="og:image" content="'.esc_attr($dp_metatags_og_image).'" />'; }
				
				if(!empty($dp_metatags_og_video)){ echo '
	<meta property="og:video" content="'.esc_attr($dp_metatags_og_video).'" />'; }
				
				if(!empty($dp_metatags_og_url)){ echo '
	<meta property="og:url" content="'.esc_attr($dp_metatags_og_url).'" />'; }
				
				if(!empty($dp_metatags_og_description)){ echo '
	<meta property="og:description" content="'.esc_attr($dp_metatags_og_description).'" />'; }
				
				
				
				if(!empty($dp_metatags_twitter_card)){ echo '
	<meta name="twitter:card" content="'.esc_attr($dp_metatags_twitter_card).'" />'; }
				
				if(!empty($dp_metatags_twitter_title)){ echo '
	<meta name="twitter:title" content="'.esc_attr($dp_metatags_twitter_title).'" />'; }
				
				if(!empty($dp_metatags_twitter_description)){ echo '
	<meta name="twitter:description" content="'.esc_attr($dp_metatags_twitter_description).'" />'; }
				
				if(!empty($dp_metatags_twitter_image)){ echo '
	<meta name="twitter:image" content="'.esc_attr($dp_metatags_twitter_image).'" />'; }
				
				
				if(!empty($dp_metatags_custom)){ 
					$allowed_html = array(
						'meta' => array(
							'name' => array(),
							'property' => array(),
							'content' => array(),						
							'http-equiv' => array()
						)
					);
					
					echo '
	'.wp_kses( $dp_metatags_custom, $allowed_html );					
				}
				
				
			}	
			
			
			echo '
	<!-- META TAGS PLUGIN END -->
			
			';
		}
		add_action('wp_head', 'dp_metatags_echo', 0);
	
	

	
	
?>