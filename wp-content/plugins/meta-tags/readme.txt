=== Meta Tags ===
Author: divpusher
Contributors: divpusher
Donate link: https://divpusher.com/
Tags: meta tags, seo, edit meta tags, search engine optimization, facebook open graph, twitter cards, schema.org
Requires at least: 4.7.0
Tested up to: 4.9.4
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A super simple plugin to edit meta tags on all your posts and pages for SEO. Facebook's OpenGraph and Twitter Cards are included.


== Description ==

A super simple plugin to edit meta tags on all your posts and pages for SEO. Facebook's OpenGraph and Twitter Cards are included.
We’d love to hear your feedbacks and suggestions, please let us know on our support forums!


== Setup ==

After activating the plugin visit your page/post editors where a new box will appear. There you can set meta tags for each page/post.
In case your frontpage shows your latest posts, go to Settings - Meta Tags to set up tags for frontpage.


== Screenshots ==

1. The meta tag editor in post/page editor


== Frequently Asked Questions ==

None yet.


== Changelog ==

= 1.2.4 =
* Fix: meta tags will now appear properly if the page is set as Posts page in Settings / Reading
* Fix: in some cases meta tag settings disappeared, now they shouldn't

= 1.2.3 =
* Fix: meta tags now appear properly on WooCommerce pages

= 1.2.2 =
* Update: added support for woocommerce products
* Fix: notices

= 1.2.1 =
* Fix: some strings were not translation ready
* Fix: support notice on plugin page now appears only once, after activation 

= 1.2.0 =
* POST inputs are now sanitized before saving
* Added nonce and permission check

= 1.1 =
* The first version of this plugin! Enjoy! :)